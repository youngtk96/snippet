function formatMoney(n, c, d, t){
  //var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
     return s + '$' + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };

  var dataRow = new Array();
  dataRow.push([" Age ", " Hourly Wage ", " Annual Pay ($)", " Weekly Payroll Deferrals ", " OSI Match ", " Bonus Contribution "," Total 401(k) Value ($) "]);
var init_annual_pay = (parseFloat(initial_hourly_wage)*2080);
var init_weekly_payroll_deferrals = Number(init_annual_pay)*(Number(weekly_contributions)/100);
var init_osi_match = 520;
var init_bonus_contribution = (Number(annual_share_osi_bonus)*(Number(bonus_contribution)/100));
var init_total_401k_value = ((Number(current_401k_val)+init_weekly_payroll_deferrals+init_osi_match+init_bonus_contribution)*(1+((Number(annual_growth_rate)/100)*0.5)));

dataRow.push([current_age, formatMoney(initial_hourly_wage, 2, '.', ','), formatMoney(init_annual_pay, 2, '.', ','), formatMoney(init_weekly_payroll_deferrals, 2, '.', ','), formatMoney(init_osi_match, 2, '.', ','), formatMoney(init_bonus_contribution, 2, '.', ','), formatMoney(init_total_401k_value, 2, '.', ',')]);

var k = 1;
do {
    var calc_age = Number(current_age)+k;

  if (k < 2) {
    var calc_hourly_wage = parseFloat(initial_hourly_wage)+(parseFloat(initial_hourly_wage)*((parseFloat(avg_annual_wage_increase)/100)));
    var prev_hourly_wage = parseFloat(initial_hourly_wage)+(parseFloat(initial_hourly_wage)*((parseFloat(avg_annual_wage_increase)/100)));
  } else {
    var calc_hourly_wage = parseFloat(prev_hourly_wage)*(1+((parseFloat(avg_annual_wage_increase)/100)));
    var prev_hourly_wage = parseFloat(calc_hourly_wage);
  };

  var calc_annual_pay = (Number(calc_hourly_wage)*2080);
    var calc_weekly_payroll_deferrals = (Number(calc_annual_pay)*(Number(weekly_contributions)/100));
    var calc_bonus_contribution = (Number(annual_share_osi_bonus)*(Number(bonus_contribution)/100));

  if (k < 2) {
    var calc_total_401k_value = (parseFloat(init_total_401k_value)*(1+((Number(annual_growth_rate)/100))))+((calc_weekly_payroll_deferrals+init_osi_match+calc_bonus_contribution)*(1+((Number(annual_growth_rate)/100)*0.5)));
    var prev_total_401k_value = (parseFloat(init_total_401k_value)*(1+((Number(annual_growth_rate)/100))))+((calc_weekly_payroll_deferrals+init_osi_match+calc_bonus_contribution)*(1+((Number(annual_growth_rate)/100)*0.5)));
  } else {
    var calc_total_401k_value = (parseFloat(prev_total_401k_value)*(1+((Number(annual_growth_rate)/100))))+((calc_weekly_payroll_deferrals+init_osi_match+calc_bonus_contribution)*(1+((Number(annual_growth_rate)/100)*0.5)));
    var prev_total_401k_value = parseFloat(calc_total_401k_value);
  };

  dataRow.push([calc_age, formatMoney(calc_hourly_wage, 2, '.', ','), formatMoney(calc_annual_pay, 2, '.', ','), formatMoney(calc_weekly_payroll_deferrals, 2, '.', ','), formatMoney(init_osi_match, 2, '.', ','), formatMoney(calc_bonus_contribution, 2, '.', ','), formatMoney(calc_total_401k_value, 2, '.', ',')]);
  k++;
}
while (k <= parseInt(years_to_retirement));
